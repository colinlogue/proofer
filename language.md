

## Sets
Example expressions that evaluate to sets:

```
{ 1, 2, 3 }
```
Explicitly states each member. Here, the numbers are parsed as
integer literals so it is a subset of **Z**.

```
{ apple, 2, -12 }
```
This set is also explicitly defined, but it has elements from
different domains.

```
{ x : odd(x) }
```
Here a predicate is used to define what elements are in the set.
Because odd is defined as `odd: Z -> truth`, the set is implicitly
a subset of **N**.

```
{ x in Z : odd(x) }
```
This is the same set as above, but with the domain explicitly stated.

```
{ x in Z where x is odd }
```
Same as previous two.

```
{ 1...4}
```
This is the same as `{ 1,2,3,4 }`.

## Samples

```
for all x in {1...3}, exists y in {2,4,6} s.t. y = multiply(2,x)
```

## Reserved words
 *  `for all`
 *  `exists`
 *  `in`
 *  `where`
 
## Reserved symbols
 *  `:`, `|`
 *  `(`, `)`
 *  `{`, `}`