# Project style guide

## General
Pretty much follows Python style guide because that's what I'm used
to.

### Layout
```typescript
// file documentation comment
// import statements
// constants and variables
// functions
// classes
// single export statement
```

### Strings
Strings should be marked by single quotes.
```typescript
let a_string = 'a string';
```

### Variables
```typescript
let example_variables = 'value';
```

### Functions
```typescript
function example_function(): void {}
```

### Classes
```typescript
class ExampleClass {
    // static data members
    static static_label = 'label';

    // static functions
    static static_func(): void {}

    // data members
    private data_member: number;

    // constructor
    constructor() {}

    // instance methods
    public example_method(): void {}
}
```
In each category, list all public followed by all protected and
finally all private. Order alphabetically within each section.

### Enums
```typescript
enum ExampleEnum {}
```

### Spacing
Two spaces between definitions.
```typescript
function func1() {
    // some stuff
}


function func2() {
    // some other stuff
}
```