# Proofer
A GUI application for building and checking proof trees. Very WIP at
the moment.

A demo is available 
[here](https://web.cecs.pdx.edu/~clogue/proofer/index.html).
