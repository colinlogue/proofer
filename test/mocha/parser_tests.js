import {parse, ParseError} from '../../scripts/language/parser';

var chai = require('chai');
var expect = chai.expect;

describe('parse tests', () => {
    // TODO: should return null expression on empty input?
    // it('throws error on empty input', () => {
    //     expect(() => parse('')).to.throw(ParseError);
    // });
    it('parses single variable', () => {
        let expr = parse('a');
        expect(expr).to.be.a('VariableNode');
    });
    it('parses "NOT a"', () => {
        let expr = parse('NOT a');
        expect(expr).to.be.a('NotNode');
        expect(expr.expr).to.be.a('VariableNode');
        expect(expr.expr.name).to.be.equal('a');
    });
    it('checks operation arity vs operands', () => {
        expect(() => parse('&& a')).to.throw(ParseError);
    });
    it('parses "a AND b"', () => {
        let expr = parse('a && b');
        expect(expr).to.be.a('AndNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('a');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('b');
    });
    it('parses "(a)"', () => {
        let expr = parse('(a)');
        expect(expr).to.be.a('VariableNode');
        expect(expr.name).to.be.equal('a');
    });
    it('checks matching brackets', () => {
        expect(() => parse('{(a})')).to.throw(ParseError);
    });
    it('nested brackets', () => {
        let expr = parse('{( b )}');
        expect(expr).to.be.a('VariableNode');
        expect(expr.name).to.be.equal('b');
    });
    it('(NOT [a && {~ b}])', () => {
        let expr = parse('(NOT [a && {~ b}])');
        expect(expr).to.be.a('NotNode');
        expect(expr.expr).to.be.a('AndNode');
        expect(expr.expr.lhs).to.be.a('VariableNode');
        expect(expr.expr.lhs.name).to.be.equal('a');
        expect(expr.expr.rhs).to.be.a('NotNode');
        expect(expr.expr.rhs.expr).to.be.a('VariableNode');
        expect(expr.expr.rhs.expr.name).to.be.equal('b');
    });
    it('&&(x,y)', () => {
        let expr = parse('&&(x,y)');
        expect(expr).to.be.a('AndNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('x');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('y');
    });
    it('parses x || y', () => {
        let expr = parse('x || y');
        expect(expr).to.be.an('OrNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('x');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('y');
    });
    it('parses "bagel -> cream_cheese"', () => {
        let expr = parse('bagel -> cream_cheese');
        expect(expr).to.be.an('ArrowNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('bagel');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('cream_cheese');
    });
    it('parses "a || ~ a', () => {
        let expr = parse('a || ~ a');
        expect(expr).to.be.an('OrNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('a');
        expect(expr.rhs).to.be.a('NotNode');
        expect(expr.rhs.expr).to.be.a('VariableNode');
        expect(expr.rhs.expr.name).to.be.equal('a');
    });
    it('parses "(a) ∧ (b)"', () => {
        let expr = parse('(a) ∧ (b)');
        expect(expr).to.be.an('AndNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('a');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('b');
    });
});