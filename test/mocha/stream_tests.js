import {TokenStream, InputStream} from "../../scripts/language/streams";

var chai = require('chai');
var expect = chai.expect;

function get_table() {
    return {
        tokens: ['string'],
        rules: [
            {pred: (c) => /\s/.test(c), reader:
                    (stream) => {
                        while (/\s/.test(stream.peek())) {
                            stream.next();
                        }
                        return null;
                    } },
            {pred: (char) => /\w/.test(char), reader:
                    (stream) => {
                        let str = "";
                        while (/\w/.test(stream.peek())) {
                            str += stream.next();
                        }
                        return {type: 'string', value: str}
                    }
            }]
    };
}

describe('TokenStream tests', function() {
    it('TokenStream constructor', function() {
        let text_str = "text_str";
        let input = new InputStream(text_str);
        let table = {tokens: [], rules: []};
        let stream = new TokenStream(input, table);
        expect(stream).to.be.a('TokenStream');
    });
    it('TokenStream.read_next() reads a single token', function() {
        let text_str = "text str";
        let table = get_table();
        let input = new InputStream(text_str);
        let stream = new TokenStream(input, table);
        let token = stream.next();
        expect(token).to.have.property('type', 'string');
        expect(token).to.have.property('value', 'text');
    });
    it('TokenStream.read_next() reads multiple tokens', function() {
        let text_str = "text str";
        let table = get_table();
        let input = new InputStream(text_str);
        let stream = new TokenStream(input, table);
        let token = stream.next();
        expect(token).to.have.property('type', 'string');
        expect(token).to.have.property('value', 'text');
        token = stream.next();
        expect(token).to.have.property('type', 'string');
        expect(token).to.have.property('value', 'str');
        token = stream.next();
        expect(token).to.be.null;
    });
    it('TokenStream reports eof after stream is exhausted', function() {
        let text_str = "text str";
        let table = get_table();
        let input = new InputStream(text_str);
        let stream = new TokenStream(input, table);
        let token;
        expect(stream.eof()).to.be.false;
        token = stream.next();
        expect(stream.eof()).to.be.false;
        token = stream.next();
        expect(stream.eof()).to.be.true;
    })
});

describe('parser tests', function() {
    it('InputStream constructor', function() {
        let test_str = 'test string';
        let stream = new InputStream(test_str);
        expect(stream).to.be.a('InputStream');
        stream = new InputStream("");
        expect(stream).to.be.a('InputStream');
    });
    it('InputStream.next()', function() {
        let test_str = 'test string';
        let stream = new InputStream(test_str);
        expect(stream.next()).to.equal('t');
        expect(stream.next()).to.equal('e');
        expect(stream.next()).to.equal('s');
        expect(stream.next()).to.equal('t');
        expect(stream.next()).to.equal(' ');
        expect(stream.next()).to.equal('s');
        expect(stream.next()).to.equal('t');
        expect(stream.next()).to.equal('r');
        expect(stream.next()).to.equal('i');
        expect(stream.next()).to.equal('n');
        expect(stream.next()).to.equal('g');
        expect(stream.next()).to.equal('');
        expect(stream.next()).to.equal('');
    });
    it('InputStream.eof()', function() {
        let test_str = "a b c";
        let stream = new InputStream(test_str);
        expect(stream.eof()).to.be.false;
        expect(stream.next()).to.equal('a');
        expect(stream.eof()).to.be.false;
        expect(stream.next()).to.equal(' ');
        expect(stream.eof()).to.be.false;
        expect(stream.next()).to.equal('b');
        expect(stream.eof()).to.be.false;
        expect(stream.next()).to.equal(' ');
        expect(stream.eof()).to.be.false;
        expect(stream.next()).to.equal('c');
        expect(stream.eof()).to.be.true;
        expect(stream.next()).to.equal('');
    });
    it('InputStream.peek()', function() {
        let test_str = "ab\" 'c(d)";
        let stream = new InputStream(test_str);
        while (!stream.eof()) {
            expect(stream.peek()).to.be.equal(stream.next());
        }
    });
});
