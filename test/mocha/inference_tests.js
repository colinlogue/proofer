import { and, not, variable } from '../../scripts/helpers';
import {parse} from "../../scripts/language/parser";
import {InferenceRule} from '../../scripts/proofs/inference';
import {Context} from "../../scripts/language/expressions";
import {AndIntro, AndElimL, AndElimR, OrIntroL, OrIntroR} from '../../scripts/proofs/rules';


var chai = require('chai');
var expect = chai.expect;

describe('inference tests', () => {
    let a, b, c;
    beforeEach( () => {
        a = variable('a');
        b = variable('a');
        c = variable('c');
    });
    it('define new InferenceRule', () => {
        let rule = new InferenceRule('TestRule', [a, b], parse('a&&b'));
        expect(rule).to.be.an('InferenceRule');
    });
    it('[] -> (a || ~ a) concludes (a || ~ a) with no premises', () => {
        let rule = new InferenceRule(
            'LEM',
            'LEM',
            [],
            parse('a || ~ a'));
        let assignments = new Map();
        assignments.set('a', 'to_be');
        let expr = rule.derive([], assignments);
        expect(expr).to.be.an('OrNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('to_be');
        expect(expr.rhs).to.be.a('NotNode');
        expect(expr.rhs.expr).to.be.a('VariableNode');
        expect(expr.rhs.expr.name).to.be.equal('to_be');
    });
});

describe('rules of inference', () => {
    let a, b, c;
    beforeEach( () => {
        a = variable('a');
        b = variable('b');
        c = variable('c');
    });
    it('AndIntro', () => {
        let expr = AndIntro.derive([a,b]);
        expect(expr).to.be.an('AndNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('a');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('b');
    });
    it('AndElimL', () => {
        let expr = AndElimL.derive([and(a,b)]);
        expect(expr).to.be.a('VariableNode');
        expect(expr.name).to.be.equal('a');
    });
    it('AndElimR', () => {
        let expr = AndElimR.derive([and(a,b)]);
        expect(expr).to.be.a('VariableNode');
        expect(expr.name).to.be.equal('b');
    });
    it('OrIntroL', () => {
        let assignments = new Map();
        assignments.set('b','b');
        let expr = OrIntroL.derive([a], assignments);
        expect(expr).to.be.an('OrNode');
        expect(expr.lhs).to.be.a('VariableNode');
        expect(expr.lhs.name).to.be.equal('a');
        expect(expr.rhs).to.be.a('VariableNode');
        expect(expr.rhs.name).to.be.equal('b');
    });
});