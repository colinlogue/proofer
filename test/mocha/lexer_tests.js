import {InputStream, TokenStream} from "../../scripts/language/streams";
import {lexer} from '../../scripts/language/lexer'

var chai = require('chai');
var expect = chai.expect;

const CON = 'CONSTANT';
const VAR = 'VARIABLE';
const OP = 'OPERATION';
const LEFT = 'BRACKET_LEFT';
const RIGHT = 'BRACKET_RIGHT';
const COMMA = 'COMMA';


describe('tokenizing tests', function() {
    it('skips whitespace', function() {
        let test_str = '  yohoo\t whoa howdy\n\nboo ';
        let stream = lexer(test_str);
        let count = 0;
        while (!stream.eof()) {
            let tok = stream.next();
            // make sure no whitespace in token name
            expect(/\s/.test(tok.value)).to.be.false;
            if (tok) ++count;
        }
        // should get 4 tokens
        expect(count).to.be.equal(4);
    });
    it('tokenizes not', function() {
        let tok_type = OP;
        let op_type = 'NOT';
        let test_str = 'NOT a';
        let stream = lexer(test_str);
        let not = stream.next();
        let a = stream.next();
        expect(not.type).to.be.equal(tok_type);
        expect(not.value).to.be.equal(op_type);
    });
    it('tokenizes and', function() {
        let tok_type = 'OPERATION';
        let op_type = 'AND';
        let test_str = 'a && b'
        let stream = lexer(test_str);
        let a = stream.next();
        let and  = stream.next();
        let b = stream.next();
        expect(and.type).to.be.equal(tok_type);
        expect(and.value).to.be.equal(op_type);
    });
    it('tokenizes or', function() {
        let tok_type = 'OPERATION';
        let op_type = 'OR';
        let test_str = 'a || b'
        let stream = lexer(test_str);
        let a = stream.next();
        let op  = stream.next();
        let b = stream.next();
        expect(op.type).to.be.equal(tok_type);
        expect(op.value).to.be.equal(op_type);
    });
    it('tokenizes arrow', function() {
        let tok_type = 'OPERATION';
        let op_type = 'ARROW';
        let test_str = 'a -> b'
        let stream = lexer(test_str);
        let a = stream.next();
        let op  = stream.next();
        let b = stream.next();
        expect(op.type).to.be.equal(tok_type);
        expect(op.value).to.be.equal(op_type);
    });
    it('tokenizes brackets', function() {
        const LEFT = 'BRACKET_LEFT';
        const RIGHT = 'BRACKET_RIGHT';
        let test_str = '{ }[[(] ()} as]';
        let stream = lexer(test_str);
        let token = stream.next();
        expect(token.type).to.be.equal(LEFT);
        expect(token.value).to.be.equal('{}');
        token = stream.next();
        expect(token.type).to.be.equal(RIGHT);
        expect(token.value).to.be.equal('{}');
        token = stream.next();
        expect(token.type).to.be.equal(LEFT);
        expect(token.value).to.be.equal('[]');
        token = stream.next();
        expect(token.type).to.be.equal(LEFT);
        expect(token.value).to.be.equal('[]');
        token = stream.next();
        expect(token.type).to.be.equal(LEFT);
        expect(token.value).to.be.equal('()');
        token = stream.next();
        expect(token.type).to.be.equal(RIGHT);
        expect(token.value).to.be.equal('[]');
        token = stream.next();
        expect(token.type).to.be.equal(LEFT);
        expect(token.value).to.be.equal('()');
        token = stream.next();
        expect(token.type).to.be.equal(RIGHT);
        expect(token.value).to.be.equal('()');
        token = stream.next();
        expect(token.type).to.be.equal(RIGHT);
        expect(token.value).to.be.equal('{}');
        token = stream.next();
        token = stream.next();
        expect(token.type).to.be.equal(RIGHT);
        expect(token.value).to.be.equal('[]');
    });
    it('tokenizes numbers', function() {
        let test_str = '1 23 587';
        let stream = lexer(test_str);
        let tok = stream.next();
        expect(tok.type).to.be.equal(CON);
        expect(tok.value).to.be.equal('1');
        tok = stream.next();
        expect(tok.type).to.be.equal(CON);
        expect(tok.value).to.be.equal('23');
        tok = stream.next();
        expect(tok.type).to.be.equal(CON);
        expect(tok.value).to.be.equal('587');
    });
    it('tokenizes variables', function() {
        let test_str = '(a && b&&c) -> Name ^Name |NAME|';
        let stream = lexer(test_str);
        let expected = [
            [LEFT, '()'],
            [VAR, 'a'],
            [OP, 'AND'],
            [VAR, 'b&&c'],
            [RIGHT, '()'],
            [OP, 'ARROW'],
            [VAR, 'Name'],
            [VAR, '^Name'],
            [VAR, '|NAME|']
        ]
        for (let x of expected) {
            let type = x[0];
            let value = x[1];
            let tok = stream.next();
            expect(tok.type).to.be.equal(type);
            expect(tok.value).to.be.equal(value);
        }
        expect(stream.eof()).to.be.true;
    });
    it('tokenizes complex expressions', function() {
        let test_str = ' ( a && b)->\nd ||\t(e)';
        let stream = lexer(test_str);
        let tok = stream.next();
        expect(tok.type).to.be.equal(LEFT);
        tok = stream.next();
        expect(tok.type).to.be.equal(VAR);
        expect(tok.value).to.be.equal('a');
        tok = stream.next();
        expect(tok.type).to.be.equal(OP);
        tok = stream.next();
        expect(tok.type).to.be.equal(VAR);
        tok = stream.next();
        expect(tok.type).to.be.equal(RIGHT);
        tok = stream.next();
        expect(tok.type).to.be.equal(OP);
        tok = stream.next();
        expect(tok.type).to.be.equal(VAR);
        tok = stream.next();
        expect(tok.type).to.be.equal(OP);
        tok = stream.next();
        expect(tok.type).to.be.equal(LEFT);
        tok = stream.next();
        expect(tok.type).to.be.equal(VAR);
        tok = stream.next();
        expect(tok.type).to.be.equal(RIGHT);
        tok = stream.next();
        expect(tok).to.be.null;
    });
    it('tokenizes comma', function() {
        let stream = lexer(',a,b,');
        let tok = stream.next();
        expect(tok.type).to.be.equal(COMMA);
        tok = stream.next();
        expect(tok.type).to.be.equal(VAR);
        tok = stream.next();
        expect(tok.type).to.be.equal(COMMA);
        tok = stream.next();
        expect(tok.type).to.be.equal(VAR);
        tok = stream.next();
        expect(tok.type).to.be.equal(COMMA);
    })
});