import { True, False, Not, Context } from '../../scripts/syntax';
import { and, or, arrow, not, variable } from '../../scripts/helpers';
import {sameType} from "../../scripts/language/expressions";

var chai = require('chai');
var expect = chai.expect;

describe('Expression values', () => {
    it('not flips truth value', () => {
        let not = new Not(True);
        expect(not.evaluate({})).to.be.false;
        not = new Not(False);
        expect(not.evaluate({})).to.be.true;
    });
    it('variables evaluate correctly', () => {
        let a = variable('a');
        let b = variable('b');
        let con = new Context();
        con.set('a', True);
        con.set('b', False);
        expect(and(a, b).evaluate(con)).to.be.false;
        expect(and(a, b).evaluate(con)).to.be.false;
        expect(or(a, b).evaluate(con)).to.be.true;
        expect(or(a, b).evaluate(con)).to.be.true;
        expect(arrow(a, b).evaluate(con)).to.be.false;
        expect(arrow(b, a).evaluate(con)).to.be.true;
        expect(not(a).evaluate(con)).to.be.false;
        expect(not(b).evaluate(con)).to.be.true;
    });
    it('variable expressions evaluate', () => {
        let con = new Context();
        let a = variable('a');
        let b = variable('b');
        let c = variable('c');
        let d = variable('d');
        con.set('a', True);
        con.set('b', False);
        con.set('c', and(a, b));
        con.set('d', or(a, b));
        expect(c.evaluate(con)).to.be.false;
        expect(not(c).evaluate(con)).to.be.true;
        expect(d.evaluate(con)).to.be.true;
        expect(arrow(False, d).evaluate(con)).to.be.true;
    });
    it('can gather variables from expression', () => {
        let a = variable('a');
        let b = variable('b');
        let c = variable('c');
        let expr1 = and(a, or(b, c));
        expect(expr1.vars).to.have.keys('a', 'b', 'c');
        let expr2 = arrow(a, a);
        expect(expr2.vars).to.have.keys('a');
        let expr3 = arrow(or(b, c), and(b, c));
        expect(expr3.vars).to.have.keys('b', 'c');
        expect(a.vars).to.have.keys('a');
    });
    it('no recursively defined variables', () => {
        let con = new Context();
        let a = variable('a');
        let b = variable('b');
        expect(() => con.set('a', and(a, b))).to.throw();
    });
    it('can substitute variables', () => {
        let a = variable('a');
        let sub_a = and(variable('c'), variable('b'));
        let context = new Context();
        context.set('a', sub_a);
        expect(a.substitute(context)).to.be.an('AndNode');
    });
    it('can compare variables', () => {
        let a = variable('a');
        let b = variable('b');
        let aa = variable('a');
        expect(a.compare(a)).to.be.true;
        expect(a.compare(b)).to.be.false;
        expect(a.compare(aa)).to.be.true;
        expect(b.compare(a)).to.be.false;
        expect(b.compare(b)).to.be.true;
        expect(aa.compare(a)).to.be.true;
    });
    it('can compare expressions', () => {
        let a = variable('a');
        let b = variable('b');
        let c = variable('c');
        let expr1 = arrow(and(a,b), or(arrow(b,c),c));
        let expr2 = arrow(and(a,b), or(arrow(c,c),c));
        expect(expr1.compare(expr1)).to.be.true;
        expect(expr1.compare(expr2)).to.be.false;
    });
});

describe('utility functions', () => {
    let a, b, c;
    let a_and_b, b_and_c, c_and_c, c_and_a;
    let a_or_a, b_or_c, b_or_a_and_b;
    let c_arrow_b, a_and_b_arrow_c;
    beforeEach( () => {
        a = variable('a');
        b = variable('b');
        c = variable('c');
        a_and_b = and(a,b);
        b_and_c = and(b,c);
        c_and_c = and(c,c);
        c_and_a = and(c,a);
        a_or_a = or(a,a);
        b_or_c = or(b,c);
        b_or_a_and_b = and(or(b,a), b);
        c_arrow_b = arrow(c,b);
        a_and_b_arrow_c = arrow(and(a,b), c);
    });
    it('sameType works for variables', () => {
        expect(sameType(a, b)).to.be.true;
        expect(sameType(b, c)).to.be.true;
    });
    it('sameType of different types is false', () => {
        expect(sameType(a, a_and_b)).to.be.false;
        expect(sameType(a_and_b, a)).to.be.false;
        expect(sameType(c_and_a, c)).to.be.false;
        expect(sameType(b_or_c, b_and_c)).to.be.false;
        expect(sameType(c_arrow_b, c_and_a)).to.be.false;
        expect(sameType(a_and_b, a_and_b_arrow_c)).to.be.false;
    });
});

describe('Context tests', () => {
    it('Context cannot have recursive definitions', () => {
        let con = new Context();
        expect(() => con.set('a', variable('a'))).to.throw();
    });
});