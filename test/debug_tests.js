import {SymbolTable, TokenStream} from "../scripts/lexer2";
import {InputStream} from "../scripts/parser";
import {table} from '../scripts/grammar';

let test_str = "var a";
let input = new InputStream(test_str);
let stream = new TokenStream(input, table);
let token1 = stream.read_next();
let token2 = stream.read_next();
let token3 = stream.read_next();
expect(token1).to.be.a("Token");
expect(token2).to.be.a("Token");
expect(token3).to.be.null;
expect(token1.type).to.equal("variable");
expect(token2.type).to.equal("variable");
expect(token1.value).to.equal("var");
expect(token2.value).to.equal("a");
