// Type definitions for proofer.language
// Project: proofer
// Definitions by: Colin Logue

type TokenType = string;
type Token = {type: TokenType, value: any}
type Predicate = (string) => boolean;
type Reader = (InputStream) => Token;
type Rule = { pred: Predicate, reader: Reader };
type SymbolTable = { tokens: TokenType[], rules: Rule[] };

interface Stream<T> {
    eof: () => boolean,
    next: () => T,
    peek: () => T
}
