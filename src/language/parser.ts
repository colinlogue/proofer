/* parser.ts

# Expression Parser

 */


import {Expression, NullExpr, Variable} from './expressions.js';
import {lexer} from './lexer.js';
import {And, Or, Not, Arrow} from './logic.js';

const EOF = {type: 'EOF', value: null};

export class Production {
    // class name
    get [Symbol.toStringTag]() { return 'Production'}

    // private data members
    private rule: string[];
    private process: (tail: Token[]) => Token;

    // constructor
    constructor(rule_str: string, process: (tail: Token[]) => Token) {
        this.rule = rule_str.split(/[\s]+/);
        this.process = process;
    }

    // public methods
    public apply(stack: Token[]) {
        // replaces all tokens that matched at end
        let pos = stack.length - this.rule.length;
        if (pos < 0) {
            let msg = "not enough tokens in stack to apply production";
            throw new ParseError(msg);
        }
        // remove trailing elements
        let tail: Token[] = [];
        while (stack.length > pos) tail.push(stack.pop());
        // tail is built in reverse, so flip when done
        tail.reverse();
        stack.push(this.process(tail));
    }

    public check(stack: Token[]): boolean {
        // only checks end of the stack because if it were at the
        // beginning it would already have encountered it in a
        // previous pass
        let pos = stack.length - this.rule.length;
        // make sure available tokens at least match length of rule
        if (pos < 0) return false;
        // check if each position matches
        for (let type of this.rule) {
            if (!(type == stack[pos].type)) return false;
            ++pos;
        }
        return true;
    }
}

const operations = {
    'NOT': Not,
    'AND': And,
    'OR': Or,
    'ARROW': Arrow
};

function operation(name: string, ...args) {
    let cls = operations[name];
    return new cls(...args);
}

let productions = [
    // variable
    new Production('VARIABLE', (tail) => {
        return {type:'expression', value: new Variable(tail[0].value)};
    }),
    // bracketed expression
    new Production('BRACKET_LEFT expression BRACKET_RIGHT', (tail) => {
        // left and right types must match
        if (!(tail[0].value == tail[2].value)) {
            let l_type = tail[0].value;
            let r_type = tail[2].value;
            let msg = `Left bracket type ${l_type} does not match right ${r_type}`;
            throw new ParseError(msg);
        }
        return {type: 'expression', value: tail[1].value};
    }),
    // expression list start
    new Production('BRACKET_LEFT expression COMMA', (tail) => {
        return {
            type: 'expr_list_start',
            value: {exprs: [tail[1].value], bracket: tail[0].value} };
    }),
    // expression list add
    new Production('expr_list_start expression', (tail) => {
        tail[0].value.exprs.push(tail[1].value);
        return tail[0];
    }),
    // expression list end
    new Production('expr_list_start BRACKET_RIGHT', (tail) => {
        // bracket types must match
        if (!(tail[0].value.bracket == tail[1].value)) {
            let l_type = tail[0].value.bracket;
            let r_type = tail[1].value;
            let msg = `Left bracket type ${l_type} does not match right ${r_type}`;
            throw new ParseError(msg);
        }
        return {type: 'expression_list', value: tail[0].value.exprs};
    }),
    // infix notation (must be binary op)
    new Production('expression OPERATION expression', (tail) => {
        let op = operation(tail[1].value, tail[0].value, tail[2].value);
        if (!(op.arity == 2)) {
            let msg = `operation ${op} has arity ${op.arity}, expected 2`;
            throw new ParseError(msg);
        }
        return {type: 'expression', value: op};
    }),
    // prefix, unary operation
    new Production('OPERATION expression', (tail) => {
        let op = operation(tail[0].value, tail[1].value);
        // check that operation is arity 1
        if (!(op.arity == 1)) {
            let msg = `operation ${op} has arity ${op.arity}, expected 1`;
            throw new ParseError(msg);
        }
        return {type: 'expression', value: op};
    }),
    // function notation
    new Production('OPERATION expression_list', (tail) => {
        let expr_list = tail[1].value;
        let op = operation(tail[0].value, ...expr_list);
        // check that operation has correct arity
        if (!(op.arity == expr_list.length)) {
            let msg = `operation ${op} has arity ${op.arity}, expected `;
            msg += `${expr_list.length}`;
            throw new ParseError(msg);
        }
        return {type: 'expression', value: op};
    })
];

function check_productions(stack: Token[]) {
    for (let production of productions) {
        if (production.check(stack)) {
            production.apply(stack);
            check_productions(stack);
            break;
        }
    }
}

export function parse(input: string): Expression {
    let stream = lexer(input);
    let stack: Token[] = [];
    do {
        let next = stream.eof() ? EOF : stream.next();
        stack.push(next);
        check_productions(stack);
    } while (!stream.eof());
    let rem = stack.pop();
    // stack must be consolidated to one value by EOF
    if (stack.length > 0) {
        throw new ParseError('reached EOF without exhausting stack');
    }
    // last thing on stack must be a value or EOF
    if (rem.type == 'EOF') return NullExpr;
    if (rem.type != 'expression') {
        throw new ParseError('parse ended without producing expression');
    }
    return rem.value;
}

export class ParseError extends Error {}