/* operations.ts

# Logical Operations
This file defines the binary logical operations.

 */

import { UnaryOperation, BinaryOperation } from './operations.js';
import {Context, Expression} from "./expressions.js";

class Not extends UnaryOperation {
    // class name
    get [Symbol.toStringTag]() { return 'NotNode' }

    public static symbol = '~';

    public duplicate(): Expression {
        return new Not(this.expr);
    }
    public evaluate(context: Context): boolean {
        return !(this.expr.evaluate(context));
    }

    toString(): string {
        return `{this.symbol}({this.expr})`;
    }
}

class And extends BinaryOperation {
    // class name
    get [Symbol.toStringTag]() { return 'AndNode' }
    public static symbols: string[] = ['∧'];

    public duplicate(): Expression {
        return new And(this.lhs, this.rhs);
    }
    public evaluate(context): boolean {
        return this.lhs.evaluate(context) &&
            this.rhs.evaluate(context);
    }
}

class Or extends BinaryOperation {
    // class name
    get [Symbol.toStringTag]() { return 'OrNode' }

    public static symbols: string[] = ['∨'];

    public duplicate(): Expression {
        return new Or(this.lhs, this.rhs);
    }
    public evaluate(context): boolean {
        return this.lhs.evaluate(context) ||
            this.rhs.evaluate(context);
    }

}

class Arrow extends BinaryOperation {
    // class name
    get [Symbol.toStringTag]() { return 'ArrowNode' }

    public static symbols: string[] = ['→'];

    public duplicate(): Expression {
        return new Arrow(this.lhs, this.rhs);
    }
    public evaluate(context): boolean {
        return !(this.lhs.evaluate(context)) ||
            this.rhs.evaluate(context);
    }
}

class Xor extends BinaryOperation {
    public static symbols: string[] = ['⊕'];

    public duplicate(): Expression {
        return new Xor(this.lhs, this.rhs);
    }
    public evaluate(context): boolean {
        let lval: boolean = this.lhs.evaluate(context);
        let rval: boolean = this.rhs.evaluate(context);
        return (lval || rval) && !(lval && rval);
    }
}

class Equivalent extends BinaryOperation {
    public static symbols: string[] = ['≡'];

    public duplicate(): Expression {
        return new Equivalent(this.lhs, this.rhs);
    }
    public evaluate(context): boolean {
        return this.lhs.evaluate(context) ===
            this.rhs.evaluate(context);
    }
}

export { Not, And, Or, Arrow, Xor, Equivalent };