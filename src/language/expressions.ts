/* syntax.ts

```
Expression  :=  Predicate(Term, Term, ...)
                Operation(Expression, Expression, ...)
                Quantifier(Variable, Expression)
Term        :=  Variable
                Constant
                Function(Term, Term, ...)
```

The operations are defined in a separate file, `operations.ts`.


*/


export function sameType(a, b) {
    let type_a = Object.prototype.toString.call(a);
    let type_b = Object.prototype.toString.call(b);
    return (type_a == type_b);
}

export class Context {
    private vars: Map<string, Expression>;
    constructor() {
        this.vars = new Map<string, Expression>();
    }
    public get(label: string): Expression {
        return this.vars.get(label);
    }
    public has(label: string): boolean {
        return this.vars.has(label);
    }
    public merge(other: Context) {
        for (let [key, value] of other.vars.entries()) {
            if (this.has(key)) {
                // if they share a key, its value must match
                if (!(this.get(key).compare(value))) {
                    let msg = `unable to unify`;
                    throw Error(msg); // TODO
                }
            } else {
                this.set(key, value);
            }
        }
    }
    public set(label: string, expr: Expression) {
        // check for recursive definitions
        if (expr.vars.has(label)) throw Error; // TODO: error type
        else this.vars.set(label, expr);
    }
}

abstract class Expression {

    // constructor
    protected constructor(
        protected children: Expression[] = null) {
        if (children == null) this.children = [];
    }

    // accessors
    /**
     * An expression is closed if it contains no free variables.
     */
    get is_closed(): boolean {
        return this.children.reduce(
            (acc, cur) => acc && cur.is_closed,
            true);
    }

    /**
     * An expression is satisfiable if there is some interpretation
     * for which it evaluates to `true`.
     */
    get is_satisfiable(): boolean {
        return false;
    }

    /**
     * An expression is valid if it evaluates to `true` for all
     * interpretations and free variable assignments.
     */
    get is_valid(): boolean {
        return false;
    }

    /**
     * @returns  The number of child nodes of the current expression.
     */
    get numChildren(): number { return this.children.length }

    /**
     * @returns A `Set` of all the variable names in the expression.
     */
    get vars(): Set<string> {
        let vars = new Set<string>();
        this.children.reduce(
            (acc, subexpr) => {subexpr.vars.forEach(
                elem => acc.add(elem)); return acc}, vars);
        return vars;
    }

    // public methods
    /**
     * Checks if `other` represents the same expression tree. Note
     * that trees that are associatively equivalent but not exactly
     * the same are not considered to be the same tree.
     * @param other  Expression object to check against.
     * @returns  `true` if both expressions are exactly the same.
     */
    public compare(other: Expression): boolean {
        return sameType(this, other) &&
            this.children.length == other.children.length &&
            this.children.reduce(
                (prv, cur, idx) => {
                    return prv && cur.compare(other.children[idx]);
                }, true);
    }
    /**
     * Using the current object as a template, attempts to create a
     * mapping from each variable in the current object's tree to a
     * subexpression of `expr`.
     * @param context
     * @param expr
     * @throws  Fails if it is not possible to unify the expressions.
     */
    public getContext(expr: Expression): Context {
        // if the nodes are not of the same type, throw error
        if (!sameType(this, expr)) {
            let msg = `Unable to set context: template node ${this} ` +
                `is different type than ${expr}`;
            throw Error(msg); // TODO: error handling
        }
        // number of children must match
        if (this.children.length != expr.children.length) {
            let msg = `Unable to set context: template has ` +
                `different number of children than expression`;
            throw Error(msg);
        }
        // attempt to unify contexts of each child
        let context = new Context();
        for (let i = 0; i < this.children.length; ++i) {
            let con = this.children[i].getContext(expr.children[i]);
            context.merge(con); // throws error if can't merge
        }
        return context;
    }
    public substitute(context: Context): Expression {
        let new_expr = this.newNode();
        for (let n = 0; n < this.numChildren; ++n) {
            new_expr.children[n] = this.children[n].duplicate().substitute(context);
        }
        return new_expr;
    }

    // abstract methods
    abstract duplicate(): Expression;
    abstract evaluate(context: Context): boolean;

    // hacks
    protected newNode() {
        // @ts-ignore
        return new this.__proto__.constructor();
    }
}

class SubstitutionError extends Error {}

class Variable extends Expression {
    // class name
    get [Symbol.toStringTag]() { return 'VariableNode'; }

    // constructor
    constructor(public name: string) { super([]) }

    // accessors
    get vars(): Set<string> {
        return new Set([this.name]);
    }

    // public methods
    /**
     * Checks whether another expression is equivalent.
     * @param other  The Expression to compare with.
     * @returns  `true` if `other` has the same name, `false`
     *     otherwise.
     */
    public compare(other: Expression): boolean {
        if (!sameType(this, other)) {
            let msg = `Cannot compare variable node with ${other}`;
            throw Error(msg); // TODO
        }
        return this.name == (other as Variable).name;
    }
    public duplicate(): Expression {
        return new Variable(this.name);
    }
    public evaluate(context: Context): boolean {
        // if variable defined in context, evaluate the expression
        // it is defined as
        if (context.has(this.name)) {
            return context.get(this.name).evaluate(context);
        }
        else {
            throw `Variable ${this.name} not in context`;
        }
    }
    /**
     * Attempts to define a context (i.e. a set of variable bindings)
     * in which the
     * @param context  The current variable bindings from earlier in
     *     the Expression tree.
     * @param expr  The Expression being compared to.
     */
    public getContext(expr: Expression, context: Context = null): Context {
        if (context == null) context = new Context();
        if (context.has(this.name)) {
            // if variable name in context already, must match value
            if (!(expr.compare(context.get(this.name)))) {
                let msg = `Variable ${this.name} already exists in ` +
                    `context with conflicting definition`;
                throw new SubstitutionError(msg);
            }
        } else {
            context.set(this.name, expr);
        }
        return context;
    }
    public substitute(context: Context): Expression {
        if (context.has(this.name)) {
            return context.get(this.name);
        }
        else return new Variable(this.name);
    }
    public toString() {
        return this.name;
    }
}

class Constant extends Expression {
    public value: boolean;
    constructor(value: boolean) {
        super([]);
        this.value = value;
    }
    public duplicate(): Expression { return new Constant(this.value); }
    public evaluate(context: Context): boolean { return this.value; }
    public toString() { return this.value.toString(); }
}

// TODO: change Expression type to union of Expression class and Null
class NullExpression extends Expression {
    constructor() { super(); }
    duplicate(): Expression { return this; }
    evaluate(context: Context): boolean { return false; }
    toString(): string { return ''; }
}

const True = new Constant(true);
const False = new Constant(false);
const NullExpr = new NullExpression();

export { True, False, NullExpr, Expression, Variable }