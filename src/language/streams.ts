/* streams.ts

Defines the InputStream and TokenStream classes, which each implement
the Stream generic interface, for string and Token respectively.

 */


export class InputStream implements Stream<string> {
    // class name
    get [Symbol.toStringTag]() { return 'InputStream'; }

    // constructor
    constructor(
        private input: String,
        private pos = 0) {
    }

    // Stream interface
    public eof() {
        return this.peek() == '';
    }

    public next() {
        return this.input.charAt(this.pos++);
    }

    public peek() {
        return this.input.charAt(this.pos);
    }
}


export class TokenStream implements Stream<Token> {
    // class name
    get [Symbol.toStringTag]() { return 'TokenStream'; }

    // private data members
    private current: Token;

    // constructor
    constructor(
        private input: InputStream,
        private table: SymbolTable) {
    }

    // Stream interface
    public eof(): boolean {
        return this.peek() == null;
    }

    public next(): Token {
        // if peek has set current, read that into temp variable tok
        let tok = this.current;
        this.current = null;
        // return peeked value or try to read next if current was null
        return tok || this.read_next();
    }

    public peek(): Token {
        // checks if there is a cached value and returns that
        // or reads the next value if no cached value
        return this.current || (this.current = this.read_next());
    }

    // instance methods
    private fail() {
        // TODO: add descriptive error
        throw Error;
    }

    private read_next(): Token {
        // check if eof
        if (this.input.eof()) return null;
        // see next char
        let char = this.input.peek();
        // check all rules in order
        for (let rule of this.table.rules) {
            if (rule.pred(char)) {
                let tok = rule.reader(this.input);
                // if reader returns a token, return from function
                if (tok) return tok;
                // otherwise, update char and continue looping
                char = this.input.peek();
            }
        }
        // check if eof again (in case file ends with skipped chars)
        if (this.input.eof()) return null;
        // fail if no matches
        this.fail();
    }
}