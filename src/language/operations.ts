import {Expression} from "./expressions.js";

/**
 * ### Term definitions
 *  * **arity**: The number of operands that an operation takes.
 *  * **operands**: The children of an operation node.
 */
abstract class Operation extends Expression {
    // class name
    /**
     * Sets the default type string for classes that inherit from
     * Operation to the string `Operation`. Derived classes can
     * set their own values.
     */
    get [Symbol.toStringTag]() { return 'Operation' }

    // constructor
    /**
     * @param operands  The children of the operation node.
     */
    constructor(
        operands: Expression[]
    ) {
        super(operands);
    }

    // accessors
    get arity(): number { return this.children.length }
    get operands(): Expression[] { return this.children }

}

abstract class UnaryOperation extends Operation {
    // constructor
    /**
     * @param expr  The sole operand of the unary operation.
     */
    constructor(
        expr: Expression
    ) {
        super([expr]);
    }

    // accessors
    get expr(): Expression { return this.children[0] }
}


/**
 * The BinaryOperation class represents an operation with exactly
 * two operands. Binary operations are somewhat special because they
 * can use infix notation, rather than prefix notation, in an
 * expression string. That is, `add(a, b)` could also be written
 * as `a add b`, or equivalently, `+(a,b)` is the same as `a + b`.
 */
abstract class BinaryOperation extends Operation {
    // constructor
    /**
     * @param lhs   The left-hand side (first operand) of the operation.
     * @param rhs   The right-hand side (second operand) of the operation.
     */
    constructor(
        lhs: Expression,
        rhs: Expression
    ) {
        super([lhs, rhs]);
    }

    // accessors
    get lhs(): Expression { return this.children[0] }
    get rhs(): Expression { return this.children[1] }
    get symbol(): string {
        // @ts-ignore
        return this.constructor.symbols[0];
    }

    // public methods
    public toString() {
        return `(${this.lhs}) ${this.symbol} (${this.rhs})`
    }
}

abstract class TernaryOperation extends Operation {

}

export {Operation, UnaryOperation, BinaryOperation}