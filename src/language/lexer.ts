/* lexer.ts

Defines the symbol table for tokenizing an input string using the
TokenStream class.

 */

import {InputStream, TokenStream} from './streams.js';

// token types
enum Type {
    BRACKET_LEFT = 'BRACKET_LEFT',
    BRACKET_RIGHT = 'BRACKET_RIGHT',
    COMMA = 'COMMA',
    CONSTANT = 'CONSTANT',
    OPERATION = 'OPERATION',
    QUANTIFIER = 'QUANTIFIER',
    VARIABLE = 'VARIABLE'
}

// operations
let operations = {
    'AND': ['&&', '∧'],
    'OR': ['||', '∨'],
    'ARROW': ['->', '→'],
    'NOT': ['NOT', '~', '¬']
};
let op_map: Map<string, string> = new Map();
for (let op in operations) {
    for (let name of operations[op]) {
        op_map.set(name, op);
    }
}

// regex definitions
let bracket_chars = /[{\[()\]}]/;
let name_chars = /[\w!@#$%^&*~\-+_=|<>∧∨→¬]/;

// predicate definitions
let is_bracket = checker(bracket_chars);
let is_comma = checker(/,/);
let is_name = checker(name_chars);
let is_whitespace = checker(/\s/);
let is_operation = (name: string) => op_map.has(name);

export var table: SymbolTable = {
    tokens: [
        Type.BRACKET_LEFT,
        Type.BRACKET_RIGHT,
        Type.COMMA,
        Type.CONSTANT,
        Type.OPERATION,
        Type.QUANTIFIER,
        Type.VARIABLE,
    ],
    rules: [
        // skip whitespace
        {pred: is_whitespace, reader: skip_while(/\s/)},
        // brackets
        {pred: is_bracket, reader: brace_reader},
        // names
        {pred: is_name, reader: name_reader},
        // comma
        {pred: is_comma, reader: comma_reader}
    ]
};

// general utilities

// returns a function that tests for the given regex
function checker(regex: RegExp): (string) => boolean {
    return (char: string) => regex.test(char);
}

// returns a function to skip stream ahead while regex test is true
function skip_while(regex: RegExp): (InputStream) => Token {
    return (stream: InputStream) => {
        while (regex.test(stream.peek())) stream.next();
        // returns null rather than void to match rule type
        return null;
    }
}

// creates a token with the given type and value
function token(type: string, value: string): Token {
    return {type: type, value: value}
}

// reader definitions

function brace_reader(stream: InputStream): Token {
    let char = stream.next();
    switch(char) {
        case('(') : return token(Type.BRACKET_LEFT, '()');
        case('[') : return token(Type.BRACKET_LEFT, '[]');
        case('{') : return token(Type.BRACKET_LEFT, '{}');
        case('}') : return token(Type.BRACKET_RIGHT, '{}');
        case(']') : return token(Type.BRACKET_RIGHT, '[]');
        case(')') : return token(Type.BRACKET_RIGHT, '()');
        // shouldn't ever reach default, but just in case
        default: throw new Error(`Unknown brace character: ${char}`);
    }
}

function comma_reader(stream: InputStream): Token {
    return {type: Type.COMMA, value: stream.next()};
}

function name_reader(stream: InputStream): Token {
    let name = '';
    while (is_name(stream.peek())) {
        name += stream.next();
    }
    let type: string;
    let value: string;
    // if name is in operations map, token is operation
    if (is_operation(name)) {
        type = Type.OPERATION;
        value = op_map.get(name);
    }
    // otherwise if starts with a number it is a number
    else if (/[0-9]/.test(name[0])) {
        type = Type.CONSTANT;
        value = Number(name).toString();
    }
    // otherwise it is a variable
    else {
        type = Type.VARIABLE;
        value = name;
    }
    return token(type, value);
}

export function lexer(str: string): TokenStream {
    return new TokenStream(new InputStream(str), table);
}