



export function set_union<T>(A: Set<T>, B: Set<T>): Set<T> {
    return new Set([...A, ...B]);
}

export function set_equality(A: Set<any>, B: Set<any>): boolean {
    // must have same number of elements
    if (A.size != B.size) return false;
    // all elements in A must be in B
    for (let a of A) {
        if (!(B.has(a))) return false;
    }
    return true;
}