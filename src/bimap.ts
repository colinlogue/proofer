/* bimap.ts

## Setting values
Value pairs are set using the `set` method. There are three possible
cases when setting values:
 1. Both of the pair values already exists in the bimap. Swap the
    mappings, e.g. for the bimap `{a ↔ 1, b ↔ 2}`, calling `set(a,2)`
    would result in the mapping `{a ↔ 2, b ↔ 1}`.
 2. One of the two values is in the bimap but the other is not. Set
    the new mapping and remove the newly orphaned element. E.g. for
    `{a ↔ 1, b ↔ 2}`, calling `set(b,3)` would result in the mapping
    `{a ↔ 1, b ↔ 3}`. Calling `set(c,1)` on this new bimap would
    result in `{c ↔ 1, b ↔ 3}`.
 3. Neither value of the pair is in the bimap already. In this case,
    add the pair to the bimap and nothing else is required. E.g., for
    `{a ↔ 1, b ↔ 2}`, calling `set(c,3)` would result in the bimap
    `{a ↔ 1, b ↔ 2, c ↔ 3}`.

 */

class Bimap<X,Y> {
    private map: Map<X,Y>;

    constructor(vals = []) {
        this.map = new Map<X,Y>(vals);
        if (!(this.validate_range())) {
            throw "Invalid range for bimap";
        }
    }

    private validate_range(): boolean {
        // verifies that each value appears only once in the range
        let seen = new Set();
        for (let val of this.map.values()) {
            if (seen.has(val)) {
                return false;
            }
            seen.add(val);
        }
        return true;
    }

    public set(x: X, y: Y): void {
        // see above for discussion of cases
        let has_x: boolean = this.has_key(x);
        let has_y: boolean = this.has_value(y);
        // case 1: both already exist in bimap - swap.
        if (has_x && has_y) {
            let temp_x = this.get_key(y);
            let temp_y = this.get_value(x);
            this.map.set(x, y);
            this.map.set(temp_x, temp_y);
        }
        // case 2a: key exists in bimap - replace its value
        else if (has_x) {
            this.map.set(x,y);
        }
        // case 2b: value exists in bimap - replace its key
        else if (has_y) {
            this.map.delete(this.get_key(y));
            this.map.set(x,y);
        }
        // case 3: neither exist - add pair
        else {
            this.map.set(x,y);
        }
    }
    public has_key(x: X): boolean {
        // checks whether the given item is already in the domain
        return this.map.has(x);
    }
    public has_value(y: Y): boolean {
        for (let val of this.map.values()) {
            if (val === y) {
                return true;
            }
        }
        return false;
    }
    get_value(x: X): Y {
        // TODO: throw error if key doesn't exist
        return this.map.get(x);
    }
    public get_key(val: Y): X {
        for (let [x,y] of this.map.entries()) {
            if (y === val) {
                return x;
            }
        }
        throw `Value {val} not found in bimap`;
    }
}

export { Bimap }