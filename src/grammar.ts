import { SymbolTable, Token } from "./lexer2.js";
import { InputStream } from "./parser.js";

let table = new SymbolTable();

// parentheses
function is_paren(char: string) {
    return /[()]/.test(char);
}
function paren_reader(stream: InputStream) {
    let char = stream.next();
    if (char == ')') {
        return new Token('rightParen', char);
    }
    else if (char == '(') {
        return new Token('leftParen', char);
    }
}
table.add_rule({pred: is_paren, reader: paren_reader});

// variables
function is_letter(char: string): boolean {
    return /\w/.test(char);
}
function var_reader(stream: InputStream): Token {
    let str = "";
    while (is_letter(stream.peek())) {
        str += stream.next();
    }
    return new Token("variable", str);
}
table.add_rule({pred: is_letter, reader: var_reader});

// and
function is_and(char: string): boolean {
    return /[&·∧]/.test(char);
}
function and_reader(stream: InputStream): Token {
    return new Token('operation:and', stream.next());
}
table.add_rule({pred: is_and, reader: and_reader});

// or
function is_or(char: string): boolean {
    return /[∨∥+]/.test(char);
}
function or_reader(stream: InputStream): Token {
    return new Token('operation:or', stream.next());
}
table.add_rule({pred: is_or, reader: or_reader});

// arrow
function is_arrow(char: string): boolean {
    return /[⇒⊃→]/.test(char);
}
function arrow_reader(stream: InputStream): Token {
    return new Token('operation:arrow', stream.next());
}
table.add_rule({pred: is_arrow, reader: arrow_reader});

export { table }