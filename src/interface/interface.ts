import {ProofNode, InferenceNode, PremiseNode} from "./elements.js";
import {InferenceRule} from "../proofs/inference.js";
import {rules} from "../proofs/rules.js";
import {html, render} from "../../node_modules/lit-html/lit-html.js";
import {Context} from "../language/expressions.js";
import {variable} from "../helpers.js";
import {parse} from "../language/parser.js";

/**
 * Mode descriptions:
 *  - Select:  Default. Can select items.
 *  - Drag:    Actively dragging an element.
 *  -
 */

enum ReplaceMode {
    Swap,
    Destroy,
    Orphan
}

enum SelectionState {
    NoSelection,
    RuleSelected,
    NodeSelected
}

enum ControlState {
    pressed,
    notPressed
}

let display: HTMLElement = null;
let activeElem: HTMLElement = null;
let selectionState: SelectionState = SelectionState.NoSelection;
let replaceMode: ReplaceMode = ReplaceMode.Destroy;
let controlState: ControlState = ControlState.notPressed;

const proof_selector = '#display proof-premise, #display proof-inference';

function enableControl(): void { controlState = ControlState.pressed; }
function disableControl(): void { controlState = ControlState.notPressed; }

function activeElemIsRule(): boolean {
    return activeElem.matches('.rule-button');
}

function activeElemIsNode(): boolean {
    return activeElem.matches('proof-inference');
}

function getActiveRule(): InferenceRule {
    if (activeElemIsRule())
        return rules.get(activeElem.dataset.rule);
    return null;
}

function setActiveElem(elem): void {
    disableActiveElem();
    elem.classList.add('active');
    activeElem = elem;
    if (activeElemIsRule()) selectionState = SelectionState.RuleSelected;
    else if (activeElemIsNode()) selectionState = SelectionState.NodeSelected;
    else disableActiveElem();
}

function disableActiveElem(): void {
    document.querySelectorAll('.active').forEach(x =>
        {x.classList.remove('active');});
    activeElem = null;
    selectionState = SelectionState.NoSelection;
}

function replaceNode(newNode: InferenceNode, oldNode: PremiseNode) {
    // check if rule output form matches node input form
    let parent = oldNode.parentElement as InferenceNode;
    try {
        let premises = parent.premises;
        premises[oldNode.pos] = newNode.rule.conclusionShape;
        parent.rule.derive(premises);
        // if able to derive, can replace
        // TODO: get a better check for this
        parent.replaceChild(newNode, oldNode);
        parent.draw();
    }
    catch (e) {
        let expectedForm = parent.rule.premiseShapes[oldNode.pos].toString();
        alert(`Cannot replace node: expected form ${expectedForm}`);
    }
}

function newInferenceNode(): InferenceNode {
    let node = document.createElement('proof-inference') as InferenceNode;
    node.rule = getActiveRule();
    return node;
}

// click event handlers
function clickDispatcher(event: Event): void {
    let target = event.target as HTMLElement;
    if (target == display) return displayClickHandler(event);
    else if (target.matches(proof_selector))
        return premiseClickHandler(event);
}
function displayClickHandler(event: Event): void {
    // if a rule is selected, place a new application of that rule
    if (selectionState === SelectionState.RuleSelected) {
        let node = newInferenceNode();
        node.rule = rules.get(activeElem.dataset.rule);
        display.appendChild(node);
    }
    disableActiveElem();
}
function premiseClickHandler(event: Event): void {
    let target = event.target as PremiseNode;
    if (selectionState === SelectionState.RuleSelected) {
        let node = newInferenceNode();
        replaceNode(node, target);
    }
    disableActiveElem();
}

function ruleButtonClickHandler(event: Event, elem: Element): void {
    if (controlState == ControlState.pressed) {
        // if Ctrl is pressed, rename variables
        let context = new Context();
        let node = elem.querySelector('proof-inference') as InferenceNode;
        let var_names = Array.from(node.rule.vars.keys());
        let renames = prompt(`Set vars: ${var_names}`, `${var_names}`);
        renames.split(',').forEach( (val, idx) => {
            context.set(var_names[idx], parse(val));
        });
        node.inputNodes.forEach( (premise) => {
            (premise as PremiseNode).substitute(context);
        });
    }
    else setActiveElem(elem);
}

// TODO: make this work and be better
function popupForm(msg: string,
                   questions: string[],
                   callback: (vals: Map<string, string>) => void): void {
    let map = new Map<string, string>();
    let template = html`
        <div class="popup-form">
            <p>${msg}</p>
            ${questions.forEach( (label) => html`
            <fieldset>
                <label>${label}</label>
                <input>
            </fieldset>
            `)}
            <button>submit</button>
        </div>`;
    let container = document.getElementById('popup');
    render(template, container);
    // container.querySelector('.popup-form').addEventListener('click')
}

function keydownHandler(event: KeyboardEvent): void {
    if (event.key == "Escape") return disableActiveElem();
    else if (event.key == "Control") return enableControl();
}
function keyupHandler(event: KeyboardEvent): void {
    if (event.key == "Control") return disableControl();
}

document.addEventListener('DOMContentLoaded', () => {
    display = document.getElementById('display');
    document.body.addEventListener('click', clickDispatcher);
    document.addEventListener('keydown', keydownHandler);
    document.addEventListener('keyup', keyupHandler);
});

export {setActiveElem, ruleButtonClickHandler};