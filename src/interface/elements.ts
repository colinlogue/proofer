import {html, render} from "../../node_modules/lit-html/lit-html.js";
import {InferenceRule} from "../proofs/inference.js";
import {Context, Expression, NullExpr} from "../language/expressions.js";
import {parse} from "../language/parser.js";
import { rules } from "../proofs/rules.js";


abstract class ProofNode extends HTMLElement {
    // class name
    get [Symbol.toStringTag]() { return 'ProofNode' }

    // constructor
    protected constructor() {
        super();
        this.attachShadow({mode: 'open'});
        this.render();
    }

    // abstract interface
    abstract draw(): void;
    abstract get drawn(): boolean;
    abstract get expr(): Expression;
    abstract render(): void;

    // custom element callbacks
    connectedCallback() {
        this.updateSlots();
    }

    // protected methods
    protected updateSlots(): void {
        // explicit slot indices must be ascending
        let idx = 0;
        for (let child of this.children) {
            if (child.hasAttribute('slot'))
                idx = Number(child.getAttribute('slot'));
            else
                child.setAttribute('slot', idx.toString());
            ++idx;
        }
    }
}

class InferenceNode extends ProofNode {
    // class name
    get [Symbol.toStringTag]() { return 'InferenceNode'; }

    // constructor
    constructor(
        ) {
        super();
    }
    connectedCallback() {
        if (this.hasAttribute('rule')) {
            this.rule = rules.get(this.getAttribute('rule'));
        }
        super.connectedCallback();
    }

    // accessors
    get conclusion(): Expression {
        return this.rule.derive(this.premises);
    }
    get drawn(): boolean {
        return this.inputNodes.reduce( (acc, elem): boolean => {
            return acc && elem.drawn;
        }, true );
    }
    get inputNodes(): ProofNode[] {
        let nodes: ProofNode[] = [];
        for (let child of this.children) {
            let idx = Number(child.getAttribute('slot'));
            nodes[idx] = (child as ProofNode);
        }
        return nodes;
    }
    get premises(): Expression[] {
        return this.inputNodes.map( (node, idx) =>
            (node as PremiseNode).isEmpty ?
                this.rule.premiseShapes[idx] : node.expr
        );
    }
    get rule(): InferenceRule {
        return rules.get(this.getAttribute('rule'));
    }
    set rule(rule: InferenceRule) {
        this.setAttribute('rule', rule.name);
        // make sure there are enough children
        while (this.children.length < rule.numPremises) {
            // TODO: optimize loop
            let child = document.createElement('proof-premise');
            this.appendChild(child);
        }
        this.updateSlots();
        // update visual
        this.draw();
    }

    // ProofNode interface
    get expr(): Expression {
        return this.conclusion;
    }
    public draw(): void {
        this.updateSlots();
        // TODO: confirm that all children are drawn
        // for (let child of this.inputNodes) {
        //     if (!(child as PremiseNode).drawn) return;
        // }
        // inputs
        let template = html`${[...Array(this.rule.numPremises).keys()].map(idx =>
            html`
            <slot name="${idx}">
                <proof-premise><!-- empty --></proof-premise>
            </slot>`
        )}`;
        render(template, this.shadowRoot.querySelector('.inputs'));
        // label
        this.shadowRoot.querySelector('.label')
            .textContent = this.rule.label;
        // output
        let output = this.shadowRoot.querySelector('.output');
        let value = this.conclusion.toString();
        console.log(value, output);
        output.textContent = this.conclusion.toString();
    }
    public render(): void {
        let template = html`
            <link rel="stylesheet" href="styles/styles.css">
            <div class="node inference">
                <div class="inputs">
                </div>
                <div class="mid">
                    <div class="label">
                    </div>
                </div>
                <div class="output">
                </div>
            </div>`;
        render(template, this.shadowRoot);
        if (this.parentElement &&
                this.parentElement.hasAttribute('render'))
            (this.parentElement as ProofNode).render();
    }
}

class PremiseNode extends ProofNode {
    // class name
    get [Symbol.toStringTag]() { return 'PremiseNode'; }

    // public data
    public drawn: boolean = false;

    constructor() {
        super();
    }
    connectedCallback() {
        super.connectedCallback();
        this.draw();
    }

    // accessors
    public get expectedShape(): Expression {
        return (this.parentElement as InferenceNode).rule.premiseShapes[this.pos];
    }
    public get pos(): number { return Number(this.getAttribute('slot')); }
    public get isEmpty(): boolean { return this.expr == NullExpr; }

    public substitute(context: Context): void {
        if (this.expr) this.expr = this.expr.substitute(context);
    }

    // ProofNode interface
    public get expr(): Expression {
        return parse(this.textContent);
    }
    public set expr(e: Expression) {
        this.textContent = e.toString();
        this.draw();
    }
    public draw(): void {
        // set first time it gets drawn
        this.drawn = true;
        if (this.expr == NullExpr) {
            if ((this.parentElement as InferenceNode).rule)
                this.textContent = (this.parentElement as InferenceNode)
                    .rule.premiseShapes[this.pos].toString();
        }
        this.shadowRoot.querySelector('.node')
            .textContent = this.expr.toString();
        if ((this.parentElement as ProofNode).draw)
            (this.parentElement as ProofNode).draw();
    }
    public render(): void {
        let classlist = 'node';
        if (this.isEmpty) classlist += ' empty';
        let template = html`
            <link rel="stylesheet" href="styles/styles.css">
            <div class="${classlist}">
                ${this.textContent}
            </div>`;
        render(template, this.shadowRoot);
    }
}

// register elements
customElements.define('proof-inference', InferenceNode);
customElements.define('proof-premise', PremiseNode);

export {ProofNode, InferenceNode, PremiseNode}