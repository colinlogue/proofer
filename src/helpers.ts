/* helpers.js

A collection of functions that make writing expressions cleaner but
doesn't affect the functionality.

 */

import { And, Or, Arrow, Not } from './language/logic.js'
import {Expression, Variable} from "./language/expressions.js";

function and(lhs: Expression, rhs: Expression): And {
    return new And(lhs, rhs);
}

function or(lhs: Expression, rhs: Expression): Or {
    return new Or(lhs, rhs);
}

function arrow(lhs: Expression, rhs: Expression): Arrow {
    return new Arrow(lhs, rhs);
}

function not(expr: Expression): Not {
    return new Not(expr);
}

function variable(name: string): Variable {
    return new Variable(name);
}

export { and, or, arrow, not, variable };