import {InputStream} from "./parser.js";


class Token {
    constructor(public type: string, public value: string) {}
    // @ts-ignore
    get [Symbol.toStringTag]() { return 'Token'; }
}


class TokenStream {
    constructor(
        private input: InputStream,
        private table: SymbolTable) {
    }

    // @ts-ignore
    get [Symbol.toStringTag]() { return 'TokenStream'; }

    private fail() {
        throw Error;
    }

    private read_next(): Token {
        // skip all skippable chars
        while (!this.input.eof() && this.table.skip(this.input.peek())) {
            this.input.next();
        }
        // check if eof
        if (this.input.eof()) return null;
        // see next char
        let char = this.input.peek();
        // check all rules in order
        for (let rule of this.table.rules) {
            if (rule.pred(char)) {
                let tok = rule.reader(this.input);
                if (tok) return tok;
            }
        }
        // fail if no matches
        this.fail();
    }
}


class SymbolTable {
    constructor(
        private skip_rule: RegExp = /\s/,
        private rule_list: Rule[] = []) {
    }

    // @ts-ignore
    get [Symbol.toStringTag]() { return 'SymbolTable'; }

    get rules(): Rule[] {
        return this.rule_list;
    }

    add_rule(pred: (string) => boolean, reader: (InputStream) => Token) {
        this.rule_list.push({pred: pred, reader: reader});
    }

    skip(char: string): boolean {
        return this.skip_rule.test(char);
    }
}


export { TokenStream, SymbolTable, Token }