import {Context, Expression, Variable} from "../language/expressions.js";
import {set_union} from "../sets.js";

export class InferenceRule {
    // class name
    get [Symbol.toStringTag]() { return 'InferenceRule' }

    // constructor
    constructor(
        public name: string,
        public label: string,
        public premiseShapes: Expression[],
        public conclusionShape: Expression
    ) {}

    // accessors
    get numPremises(): number { return this.premiseShapes.length; }
    get vars(): Set<string> {
        let vars: Set<string> = new Set();
        for (let expr of this.premiseShapes) {
            vars = set_union(vars, expr.vars);
        }
        vars = set_union(vars, this.conclusionShape.vars);
        return vars;
    }

    // public methods
    /**
     * Applies the rule to a list of premises to derive a conclusion.
     * @param premises  An ordered list of the premises from which to
     *     derive the conclusion. Must match the forms defined by
     *     `premiseShapes`.
     * @param assignments
     * @throws  Fails if any of the Expression objects in `premises`
     *     does not match the form defined in `premiseShapes`.
     * @throws  Fails if any variable in `premiseShapes` has more
     *     than one definition in `premises`.
     * @returns  An Expression representing the conclusion drawn from
     *     applying the rule to the given premises. Will match the
     *     form defined by `conclusionShape`.
     */
    public derive(premises: Expression[], assignments: Map<string, string> = null): Expression {
        // assignment keys must be in conclusionShape
        if (assignments) {
            let final_vars = this.conclusionShape.vars;
            for (let name of assignments.keys()) {
                if (!(final_vars.has(name))) {
                    let msg = `Variable ${name} assigned to but does ` +
                        `not exist in conclusion ${this.conclusionShape}`;
                    throw Error(msg); // TODO: better errors
                }
            }
        }
        else assignments = new Map<string, string>();
        // must have same number of premises as rule has shapes
        if (premises.length != this.premiseShapes.length) {
            let msg = `Number of premises doesn't match rule`;
            throw Error(msg);
        }
        // if variable names from shapes are in premises, rename
        let vars = new Set();
        premises.forEach( val => vars = set_union(vars, val.vars));
        // add in assignment variables to set
        for (let name of assignments.keys()) {
            vars.add(name);
        }
        // the template variables should not have the same name as
        // any variables in the premises or in the passed-in
        // assignments
        let sub_names = new Context();
        for (let name of this.vars) {
            // add underscores to front of variable name until it
            // no longer matches
            let sub_name = name;
            while (vars.has(sub_name)) {
                sub_name = '_' + sub_name;
            }
            // only rename if necessary
            if (!(sub_name == name))
                sub_names.set(name, new Variable(sub_name));
        }
        let sub_assignments = new Context();
        for (let [name, value] of assignments) {
            // @ts-ignore
            //  TODO: make it so name is Variable rather than Expression
            let key = sub_names.has(name) ? sub_names.get(name).name : name;
            sub_assignments.set(key, new Variable(value));
        }
        let context = new Context();
        let premise_shapes = this.premiseShapes.map(
            val => val.substitute(sub_names));
        // this throws error if unable to merge. TODO: handle better
        premise_shapes.forEach( (shape, idx) => {
            context.merge(shape.getContext(premises[idx]));
        });
        let conclusion_shape = this.conclusionShape.substitute(sub_names);
        return conclusion_shape.substitute(context).substitute(sub_assignments);
    }
}

/**
 * Represents a particular use of a rule of inference.
 */
export class Inference {
    // class name
    get [Symbol.toStringTag]() { return 'Inference' }

    // constructor
    constructor(
        private rule: InferenceRule
    ) {}

    // accessors
    get conclusion(): Expression {
        return null;
    }
}