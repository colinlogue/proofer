import {InferenceRule} from "./inference.js";
import {and, arrow, or, variable} from "../helpers.js";

let a = variable('a');
let b = variable('b');
let c = variable('c');

let AndIntro = new InferenceRule('AndIntro', '∧I', [a, b], and(a,b));
let AndElimL = new InferenceRule('AndElimL', '∧EL', [and(a,b)], a);
let AndElimR = new InferenceRule('AndElimR', '∧ER', [and(a,b)], b);
let OrIntroL = new InferenceRule('OrIntroL', '∨IL', [a], or(a,b));
let OrIntroR = new InferenceRule('OrIntroR', '∨IR', [b], or(a,b));
let OrElim   = new InferenceRule('OrElim', '∨E', [arrow(a,c), arrow(b,c), or(a,b)], c);

let rules = new Map<string, InferenceRule>();
rules.set('AndIntro', AndIntro);
rules.set('AndElimL', AndElimL);
rules.set('AndElimR', AndElimR);
rules.set('OrIntroL', OrIntroL);
rules.set('OrIntroR', OrIntroR);
rules.set('OrElim', OrElim);

export {rules, AndIntro, AndElimL, AndElimR, OrIntroL, OrIntroR, OrElim}