import {rules} from "./proofs/rules.js";
import {html, render} from "../node_modules/lit-html/lit-html.js";
import * as Interface from "./interface/interface.js";

function buildRulesBox() {
    // create rules box
    let rulesBox = html`
    <div>
        ${Array.from(rules.values()).map( rule => html`
        <div class="rule-button" id="${rule.name}-button" data-rule="${rule.name}">
            <proof-inference rule="${rule.name}">
                ${rule.premiseShapes.map( expr => html`
                <proof-premise> ${expr} </proof-premise>`)}
            </proof-inference>
        </div>`)}
    </div>`;
    let parent = document.getElementById('rules-box');
    render(rulesBox, parent);
    parent.querySelectorAll('.rule-button').forEach( elem => {
        elem.addEventListener('click', event => {
            Interface.ruleButtonClickHandler(event, elem);
        });
    });
}

buildRulesBox();